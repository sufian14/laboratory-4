import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Array {
	public static void main(String[] args) {
		Random generator = new Random();
		int i = generator.nextInt(10) + 1;
		List<Integer> list = new ArrayList<>();
		for (int a = 0; a < 10000; a++) {
			list.add(i);
		}
		long startTime = System.currentTimeMillis();
		for (int a = 0; a < 1000; a++) {
			list.add(0, i);
		}
		long estimatedTime = System.currentTimeMillis() - startTime;
		System.out.println("Estimated time: " + estimatedTime);
	}
}
